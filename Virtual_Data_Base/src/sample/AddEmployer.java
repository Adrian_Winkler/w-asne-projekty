package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class AddEmployer extends Application {
    public static void main(String[] args){
        launch(args);
    }

    public TextField nameEmployer, surnameEmployer, phoneEmployer, dOBEmployer,emailEmployer,salaryEmployer;
    private Button add, backButton;
    private Label labelPleaseFill, labelSuccess;
    private Label labelName, labelSurname, labelPhone, labelDOB, labelSalary, labelEmail;
    StackPane root;

    static private Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Virtual Data Base");
        labelPleaseFill = new Label("Please, fill this sheet!");
        labelPleaseFill.setStyle("-fx-font-size: 30px; -fx-text-fill: white");
        labelPleaseFill.setTranslateY(-220);

        labelName = new Label("Name: ");
        labelSurname = new Label("Surname: ");
        labelEmail = new Label("Email: ");
        labelDOB = new Label("Date of Birth: ");
        labelSalary = new Label("Salary: ");
        labelPhone = new Label("Phone: ");

        nameEmployer = new TextField();
        nameEmployer.setPromptText("Name");
        surnameEmployer = new TextField();
        surnameEmployer.setPromptText("Surname");
        phoneEmployer = new TextField();
        phoneEmployer.setPromptText("Phone");
        emailEmployer = new TextField();
        emailEmployer.setPromptText("E-mail");
        salaryEmployer = new TextField();
        salaryEmployer.setPromptText("Salary");
        dOBEmployer = new TextField();
        dOBEmployer.setPromptText("Date of Birth");

        VBox vBox = new VBox();
        vBox.setScaleX(2.2);
        vBox.setScaleY(2.2);

        VBox vBox2 = new VBox();
        vBox2.setTranslateX(100);
        vBox2.setTranslateY(-227);
        vBox2.setScaleX(1.4);
        vBox2.setScaleY(1.4);

        labelSuccess = new Label("Success, correctly added new employer!");
        labelSuccess.setStyle("-fx-font-size: 30px; -fx-text-fill: lightgreen");
        labelSuccess.setTranslateY(200);


        HBox hBox = new HBox();
        hBox.getChildren().addAll(vBox, vBox2);

        vBox.getChildren().addAll(labelName, labelSurname, labelPhone, labelDOB, labelEmail, labelSalary);
        vBox2.getChildren().addAll(nameEmployer, surnameEmployer, phoneEmployer, dOBEmployer, emailEmployer, salaryEmployer);

        hBox.setTranslateX(300);
        hBox.setTranslateY(520);

        add = new Button("Add...");
        add.setMinSize(150, 50);
        add.setStyle("-fx-font-size: 15px; -fx-background-color: blue; -fx-text-fill: white");
        add.setTranslateY(253);
        add.setOnAction(e-> {try{goAdd();}catch (Exception error){System.out.println("Find Employer - error");}});

        backButton = new Button("Back");
        backButton.getStyleClass().add("find");
        backButton.setTranslateX(-380);
        backButton.setTranslateY(250);
        backButton.setMinSize(100, 60);

        root = new StackPane();
        root.getChildren().addAll(labelPleaseFill, hBox, add, backButton);
        root.getStyleClass().add("root-third");
        backButton.setOnAction(e -> GoBack.goBack(window));
        Scene scene = new Scene(root, 900, 600);
        scene.getStylesheets().add("Style.css");
        window.setScene(scene);
        window.setResizable(false);
        window.show();
    }

    private void goAdd() throws Exception {
        BaseOfEmployer.addNewEmployer(nameEmployer.getText(), surnameEmployer.getText(), phoneEmployer.getText(), salaryEmployer.getText(), dOBEmployer.getText(), emailEmployer.getText());
        root.getChildren().add(labelSuccess);


    }
}
