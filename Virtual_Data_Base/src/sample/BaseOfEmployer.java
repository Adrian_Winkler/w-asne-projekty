package sample;

import javafx.stage.Stage;
import java.util.Vector;

public class BaseOfEmployer {

    private static Vector<Employer> vectorOfEmployer = new Vector<>();

    public void findEmployer(String name, Stage window){
        for(Employer employer : vectorOfEmployer){
            if (employer.getName().equals(name)){
                employer.callViewEmployer(window);
            }
        }
    }
    public static boolean deleteEmployer(String name){
        for(Employer employer : vectorOfEmployer){
            if (employer.getName().equals(name)){
                vectorOfEmployer.remove(employer);
                return true;
            }
        }
        return false;
    }
    public static void addNewEmployer(String name, String surname, String phone, String salary, String dateOfBirth, String email){
        Employer newEmployer = new Employer(name, surname, phone, salary, dateOfBirth, email);
        vectorOfEmployer.add(newEmployer);
    }
    public BaseOfEmployer() {
        vectorOfEmployer.add(new Employer("Adrian", "Winkler", "608 804 938", "$10 000", "Cracow", "adijx60@gmail.com" ));
    }
}
