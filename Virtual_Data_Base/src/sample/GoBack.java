package sample;

import javafx.stage.Stage;

public class GoBack {

    static public void goBack(Stage window){
        ThirdWindow thirdWindow = new ThirdWindow();
        try {
            thirdWindow.start(window);
        }catch (Exception error){
            System.out.println(error);
        }
    }

}
