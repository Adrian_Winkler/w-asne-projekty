package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

public class Employer {


    private String name, surname, phone, salary, dOB, email;
    private String nameText = "Name", surnameText = "Surname", phoneText = "Phone", salaryText = "Salary", emailText = "E-mail", dOBText = "Date of Birth";
    private ObservableList<TableRow> tableRows = FXCollections.observableArrayList();

    public Employer(){
        this.name = "";
        this.surname = "";
        this.phone = "";
        this.salary = "";
        this.email = "";
        this.dOB = "";
        addToList();
    }


    public Employer(String name, String surname, String phone, String salary, String dateOfBirth, String email ){
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.salary = salary;
        this.dOB = dateOfBirth;
        this.email = email;
        addToList();
    }

    public void callViewEmployer(Stage window){
        ViewEmployer viewEmployer = new ViewEmployer();
        viewEmployer.setObservableList(tableRows);
        try {
            viewEmployer.start(window);
        }catch (Exception error){
            System.out.println("Employer Error");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmailText() {
        return emailText;
    }

    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }

    public String getdOBText() {
        return dOBText;
    }

    public void setdOBText(String dOBText) {
        this.dOBText = dOBText;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    private void addToList(){
        tableRows.add(new TableRow(nameText, name));
        tableRows.add(new TableRow(surnameText, surname));
        tableRows.add(new TableRow(phoneText, phone));
        tableRows.add(new TableRow(salaryText, salary));
        tableRows.add(new TableRow(emailText, email));
        tableRows.add(new TableRow(dOBText, dOB));
    }

}
