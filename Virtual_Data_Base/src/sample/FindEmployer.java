package sample;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


public class FindEmployer extends Application {
    public static void main(String[] args){
        launch(args);
    }

    static public TextField nameEmployer;
    static private Button find;
    static private Label enterEmployerName;
    static private Button backButton;
    static private Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Virtual Data Base");
        enterEmployerName = new Label("Please, enter employer name, who you want to find");
        enterEmployerName.setStyle("-fx-font-size: 30px; -fx-text-fill: white");
        enterEmployerName.setTranslateY(-220);
        find = new Button("Find...");
        find.setMinSize(300, 100);
        find.setStyle("-fx-font-size: 30px; -fx-background-color: blue; -fx-text-fill: white");
        find.setTranslateY(70);
        find.setOnAction(e-> {try{goFind();}catch (Exception error){System.out.println("Find Employer - error");}});
        nameEmployer = new TextField();
        nameEmployer.setPromptText("Enter Employer Name");
        nameEmployer.setMaxSize(500, 100);
        nameEmployer.setStyle("-fx-font-size: 30px");
        nameEmployer.setTranslateY(-100);
        nameEmployer.setAlignment(Pos.CENTER);
        nameEmployer.setOnAction(e->System.out.println(nameEmployer.getText()));
        backButton = new Button("Back");
        backButton.getStyleClass().add("find");
        backButton.setTranslateX(-380);
        backButton.setTranslateY(250);
        backButton.setMinSize(100, 60);
        StackPane root = new StackPane();
        root.getChildren().addAll(enterEmployerName, nameEmployer,find, backButton);
        root.getStyleClass().add("root-third");
        backButton.setOnAction(e -> GoBack.goBack(window));
        Scene scene = new Scene(root, 900, 600);
        scene.getStylesheets().add("Style.css");
        window.setScene(scene);
        window.setResizable(false);
        window.show();


    }

    private void goFind() throws Exception {

        BaseOfEmployer baseOfEmployer = new BaseOfEmployer();
        baseOfEmployer.findEmployer(nameEmployer.getText(), window);

    }
}
