package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.awt.*;


public class SecondWindow extends Application {

    static private boolean flag = false;
    static private Stage stageSecond;
    Scene scene2;
    Label labelPleaseLogin, labelUsername, labelPassword;
    private static TextField username;
    private static PasswordField password;
    Image imageAccount;
    ImageView imageViewAccount;
    private static VBox vBox;
    private static Label text = new Label("Unfortunately username or/and password are incorrect! Try again!");

    public static void main(String[] args){
        launch(args);
    }


    @Override
    public void start(Stage stage) throws Exception {
        stageSecond = stage;
        stage.setTitle("Virtual Data Base");
        Rectangle rect = new Rectangle();
        rect.height = 300;
        rect.width = 300;
        rect.setLocation(800, 450);
        Button buttonLogIn = new Button("Log In");
        imageAccount = new Image("file:b.png");
        imageViewAccount = new ImageView(imageAccount);
//        imageViewAccount.relocate(500, 500);
//        Group root = new Group();
        username = new TextField();
        username.setMaxWidth(200);
        password = new PasswordField();
        password.setMaxWidth(200);
        username.setPromptText("Username");
        password.setPromptText("Password");
        labelPleaseLogin = new Label("Please enter your username and password:");
        labelPleaseLogin.setStyle("-fx-font-size: 20px");
        labelPassword = new Label("Password:");
        labelPassword.setStyle("-fx-text-fill: black");
        labelPassword.setText("Password");
        labelUsername = new Label("Username:");
        labelUsername.setStyle("-fx-text-fill: black");
        buttonLogIn.getStyleClass().add("button-blue");
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10,10,10,10));
        gridPane.setHgap(10);
        gridPane.setVgap(8);
        gridPane.setAlignment(Pos.CENTER);
        vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        GridPane.setConstraints(username, 1, 0);
        GridPane.setConstraints(labelUsername, 0, 0);
        GridPane.setConstraints(password, 1, 1);
        GridPane.setConstraints(labelPassword, 0, 1);
        gridPane.getChildren().addAll(username, password);
        vBox.getChildren().addAll(labelPleaseLogin,gridPane,buttonLogIn);

        StackPane layout = new StackPane();
        layout.getChildren().addAll(vBox);

        buttonLogIn.setOnAction(e-> logInAction());
        layout.getStyleClass().add("label-image");
//        layout.setStyle("-fx-background-color: lightblue");
        scene2 = new Scene(layout, 1600, 900);
        scene2.getStylesheets().add("Style.css");
        stageSecond.setScene(scene2);
        stageSecond.show();
    }

    static private void logInAction(){
        text.setStyle("-fx-font-size: 19px; -fx-text-fill: red");
        LogIn login = new LogIn();
        boolean logInCorrect =  login.login(username.getText(), password.getText());
        if(vBox.getChildren().contains(text)){
            vBox.getChildren().remove(text);}
        if (!logInCorrect)
            vBox.getChildren().add(text);
        else{
            ThirdWindow thirdWindow = new ThirdWindow();
            try {
                thirdWindow.start(stageSecond);
            }catch (Exception error){
                System.out.println(error);
            }
        }
        }
    }


