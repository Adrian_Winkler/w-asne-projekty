package sample;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;



public class ThirdWindow extends Application {

    Button buttonAdd;
    Button buttonDelete;
    Button buttonView;
    Button buttonSearch;
    Label labelAction;

    static Stage window;

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage tStage) throws Exception {
        window = tStage;
        window.setTitle("Virtual Data Base");

        buttonAdd = new Button("Add Employer");
        buttonDelete = new Button("Delete Employer");
        buttonView = new Button("Scroll Base");
        buttonSearch = new Button("Find Employer");
        buttonAdd.getStyleClass().add("buttonChoose");
        buttonDelete.getStyleClass().add("buttonChoose");
        buttonView.getStyleClass().add("buttonChoose");
        buttonSearch.getStyleClass().add("buttonChoose");

        buttonSearch.setMinSize(200, 100);
        buttonDelete.setMinSize(200, 100);
        buttonAdd.setMinSize(200, 100);
        buttonView.setMinSize(200, 100);

        buttonAdd.setOnAction(e->goAdd());

        labelAction = new Label();
        labelAction.setText("What would you like to do?");
        labelAction.setTranslateX(0);
        labelAction.setTranslateY(0);
        labelAction.setStyle("-fx-text-fill: white; -fx-font-size: 40px");

        GridPane gridPane = new GridPane();
        gridPane.setVgap(300);
        gridPane.setHgap(300);
        GridPane.setConstraints(buttonAdd, 0, 0);
        GridPane.setConstraints(buttonDelete, 1, 0);
        GridPane.setConstraints(buttonView, 0, 1);
        GridPane.setConstraints(buttonSearch, 1, 1);
        gridPane.getChildren().addAll(buttonAdd, buttonDelete, buttonSearch, buttonView);
        gridPane.setAlignment(Pos.CENTER);

        buttonSearch.setOnAction(e->goSearch());
        buttonDelete.setOnAction(e->goDelete());
        StackPane layout = new StackPane();
        layout.getChildren().addAll(labelAction, gridPane);
        layout.getStyleClass().add("root-third");
        Scene scene = new Scene(layout, 1600, 900);
        scene.getStylesheets().add("Style.css");
        window.setScene(scene);
        window.setResizable(false);
        window.show();
    }

    private void goAdd() {
        AddEmployer addEmployer = new AddEmployer();
        try{
            addEmployer.start(window);
        }catch (Exception error){
            System.out.println(error);
        }

    }

    private void goDelete() {
        DeleteEmployer deleteEmployer = new DeleteEmployer();
        try {
            deleteEmployer.start(window);
        }catch (Exception error){
            System.out.println(error);
        }
    }


    static void goSearch(){
        FindEmployer findEmployer = new FindEmployer();
        try {
            findEmployer.start(window);
        }catch (Exception error){
            System.out.println(error);
        }
    }

}
