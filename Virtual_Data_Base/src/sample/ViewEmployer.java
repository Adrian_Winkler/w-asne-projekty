package sample;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class ViewEmployer extends Application {

    Stage window;
    Scene scene;
    Button button, backButton;
    TableView<TableRow> table;
    TextField nameInput;
    ChoiceBox<String> choiceBox;
    private ObservableList<TableRow> tableRows = FXCollections.observableArrayList();

    public static void main(String[] args){
        launch(args);
    }

    public void setObservableList(ObservableList<TableRow> takenObservableList){
        this.tableRows = takenObservableList;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Virtual Data Base");
        button = new Button("Click me");
        backButton = new Button("Back");
        backButton.setStyle("-fx-background-color: blue;-fx-text-fill: white;");
        backButton.setOnAction(e->GoBack.goBack(window));
//        Name column
        TableColumn<TableRow, String> parameterColumn = new TableColumn<>("Parameter");
        parameterColumn.setMinWidth(246);
        parameterColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        parameterColumn.getStyleClass().add("black-column");

//        Price column
        TableColumn<TableRow, String> valueColumn = new TableColumn<>("Value");
        valueColumn.setMinWidth(246);
        valueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));
        valueColumn.getStyleClass().add("black-column2");

//        Name Input
        nameInput = new TextField();
        nameInput.setPromptText("Value");
        nameInput.setMinWidth(100);

        Image img = new Image("file:zdjecie.jpg");
        ImageView imageView = new ImageView(img);
        imageView.setScaleX(0.2);
        imageView.setScaleY(0.2);
        imageView.setTranslateX(-624);
        imageView.setTranslateY(-1301);

        choiceBox = new ChoiceBox<>();
//        getItems returns the ObservableList object wchich you can add items to
        choiceBox.getItems().add("Name");
        choiceBox.getItems().add("Surname");
        choiceBox.getItems().add("Phone");
        choiceBox.getItems().addAll("DoB", "Place", "Salary", "Occupation");
//        Set the default value
        choiceBox.setValue("Name");

//        Listen for selection changes
        choiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> System.out.println(newValue));

//        Button
        Button addButton = new Button("Add");
        addButton.setOnAction(e-> addButtonClicked());
        addButton.setStyle("-fx-background-color: blue; -fx-text-fill: white;");
        Button deleteButton = new Button("Delete");
        deleteButton.setStyle("-fx-background-color: blue;-fx-text-fill: white;");
        deleteButton.setOnAction(e -> deleteButtonClicked());

        HBox hBox = new HBox();
        hBox.setPadding(new Insets(10,10,10,10));
        hBox.setSpacing(10);
        hBox.getChildren().addAll(backButton,nameInput,choiceBox, addButton, deleteButton);
        hBox.setTranslateY(320);
        hBox.setTranslateX(300);
        hBox.setScaleX(1.6);
        hBox.setScaleY(1.6);

        table = new TableView<>();
        table.setItems(getProduct());
        table.getColumns().addAll(parameterColumn, valueColumn);
        table.setMaxWidth(494);
        table.setMinHeight(380);
        table.setTranslateX(283);
        table.setTranslateY(-20);
        table.setFixedCellSize(40);
        VBox layout = new VBox(10);
        layout.setPadding(new Insets(20,20,20,20));
        layout.getChildren().addAll(table, hBox, imageView);
        layout.getStyleClass().add("root-third");
        scene = new Scene(layout, 800, 800);
        scene.getStylesheets().add("Style.css");
        window.setScene(scene);
        window.setResizable(false);
        window.show();
    }

    //    Delete button clicked
    private void deleteButtonClicked() {
        ObservableList<TableRow> productSelected, allProducts;
        allProducts = table.getItems();
        productSelected = table.getSelectionModel().getSelectedItems();

        productSelected.forEach(allProducts::remove);

    }
    // Add button clicked
    private void addButtonClicked() {
        TableRow newTableRow = new TableRow(choiceBox.getValue(), nameInput.getText());
        table.getItems().add(newTableRow);
    }
//    //      Get all of the product
    public ObservableList<TableRow> getProduct(){
        return tableRows;
    }

}
