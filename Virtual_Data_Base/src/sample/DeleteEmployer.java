package sample;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


public class DeleteEmployer extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    public TextField nameEmployer;
    private Button delete;
    private Label enterEmployerName, labelSuccess, labelFailed;
    private Button backButton;
    private StackPane root;
    private Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Virtual Data Base");
        enterEmployerName = new Label("Please, enter employer name, who you want to DELETE...");
        enterEmployerName.setStyle("-fx-font-size: 30px; -fx-text-fill: white");
        enterEmployerName.setTranslateY(-190);

        labelSuccess = new Label("Success, correctly deleted employer!");
        labelSuccess.setStyle("-fx-font-size: 30px; -fx-text-fill: lightgreen");
        labelSuccess.setTranslateY(200);

        labelFailed = new Label("No such employer in base!");
        labelFailed.setStyle("-fx-font-size: 30px; -fx-text-fill: red");
        labelSuccess.setTranslateY(200);


        delete = new Button("Delete...");
        delete.setMinSize(200, 70);
        delete.getStyleClass().add("find");
        delete.setTranslateY(245);
        delete.setOnAction(e->goDelete());
        nameEmployer = new TextField();
        nameEmployer.setPromptText("Enter Employer Name");
        nameEmployer.setMaxSize(400, 80);
        nameEmployer.setStyle("-fx-font-size: 26px");
        nameEmployer.setTranslateY(-50);
        nameEmployer.setAlignment(Pos.CENTER);
        backButton = new Button("Back");
        backButton.getStyleClass().add("find");
        backButton.setTranslateX(-380);
        backButton.setTranslateY(250);
        backButton.setMinSize(100, 60);
        backButton.setOnAction(e -> GoBack.goBack(window));
        root = new StackPane();
        root.getChildren().addAll(enterEmployerName, nameEmployer, delete, backButton);
        root.getStyleClass().add("root-third");
        Scene scene = new Scene(root, 900, 600);
        scene.getStylesheets().add("Style.css");
        window.setScene(scene);
        window.setResizable(false);
        window.show();
    }

    private void goDelete() {
        boolean flagOfDelete;
        flagOfDelete = BaseOfEmployer.deleteEmployer(nameEmployer.getText());
        if(flagOfDelete){
            if (!root.getChildren().contains(labelSuccess))
                    root.getChildren().add(labelSuccess);
        }
        else{
            if (!root.getChildren().contains(labelFailed))
                     root.getChildren().add(labelFailed);
        }
    }
}