package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;


public class Database extends Application {

    Button buttonContinue;
    Label labelVirtualBase;
    Button buttonClose;
    Stage window;
    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Virtual Data Base");
        window.setOnCloseRequest(e-> {
            e.consume();
            closeProgram();
        });
        buttonClose = new Button("Close Program");

        Image img = new Image("File:cc.png");
        ImageView imageView = new ImageView(img);
        Group root = new Group();
        buttonContinue = new Button("Continue...");
        buttonContinue.setLayoutY(780);
        buttonContinue.setLayoutX(1370);
        buttonContinue.setScaleX(2);
        buttonContinue.setScaleY(2);
        buttonContinue.setStyle("-fx-background-color: blue; -fx-text-fill: #ffffff; -fx-font-weight: bold;");
        buttonContinue.setOnAction(e-> {
            SecondWindow secondWindow = new SecondWindow();
            try {
                secondWindow.start(window);
            }catch (Exception error){
                System.out.println(error);
            }
        });
        labelVirtualBase = new Label("Virtual Data Base");
        labelVirtualBase.setLayoutX(750);
        labelVirtualBase.setLayoutY(100);
//        labelVirtualBase.setAlignment(Pos.CENTER);
        labelVirtualBase.setScaleX(5);
        labelVirtualBase.setScaleY(5);
        labelVirtualBase.setStyle("-fx-text-fill: #ffffff; -fx-font-family: 'Sitka Display'");
        root.getChildren().addAll(imageView, buttonContinue, labelVirtualBase);
        Scene scene = new Scene(root, 1600, 900);
        window.setScene(scene);
        window.setResizable(false);
        window.show();

    }
    private void closeProgram() {
        Boolean answer = ConfirmBox.display("Virtual Data Base", "Sure you want to exit?");
        if (answer)
            window.close();
    }
}
