// Created by WINKLER on 30.10.2019.

#ifndef TSP_TSP_INCLUDE_HPP
#define TSP_TSP_INCLUDE_HPP

#include <cmath>
#include <functional>
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <utility>
#include <cfloat>
#include <algorithm>

extern const double INF;
extern int UINF;

using std::cout;
using matrix = std::vector<std::vector<double>>;
using ordinate_of_zeros = std::vector<std::vector<int>>;
using vector_of_solution = std::vector<std::vector<int>>;
using solv_vect = std::vector<int>;
using map_solv  = std::vector<std::pair<int, int>>;

class TSP_cost_matrix{
private:
    int num_of_summits_;
    double low_bound_ = 0;
    matrix cost_matrix_;
    solv_vect sol_;
    solv_vect solution_;
    double the_lowest_;
    ordinate_of_zeros vec_of_zer_;
    solv_vect vector_low_bound_;
    map_solv solve_all_;
    solv_vect the_final_solution__;
public:
    TSP_cost_matrix(matrix cost_matrix);
    void find_best_path();
    bool found_zero();
    solv_vect get_the_solution();
    void reduce_row(int row);
    void reduce_all_rows();
    void reduce_col(int col);
    void reduce_all_cols();
    int get_path_cost(int row_num, int col_num);
    void find_next_path();
};

solv_vect tsp(matrix cost_matrix);
double get_forbidden_cost();
#endif //TSP_TSP_INCLUDE_HPP
