//Created by Adrian Winkler, 302936 on 30.10.2019

#include "tsp.hpp"

const double INF = DBL_MAX;
int UINF = static_cast<int>(INF);

solv_vect tsp(matrix cost_matrix){
    TSP_cost_matrix tsp_object(cost_matrix);
    solv_vect vector_of_short = tsp_object.get_the_solution();
    return vector_of_short;
}

double get_forbidden_cost(){
    return INF;
}
TSP_cost_matrix::TSP_cost_matrix(matrix cost_matrix): cost_matrix_(cost_matrix){
        num_of_summits_= static_cast<int>(cost_matrix_.size());
        this->find_best_path();
}
void TSP_cost_matrix::find_best_path(){
    reduce_all_rows();
    reduce_all_cols();
    find_next_path();
}
solv_vect TSP_cost_matrix::get_the_solution(){
    return the_final_solution__;
}
bool TSP_cost_matrix::found_zero(){
    bool found;
    std::vector<bool> found_vector;
    for(int t = 0; t < num_of_summits_; ++t){
        found_vector.clear();
        for(int k = 0; k < num_of_summits_; ++k){
            if(cost_matrix_[t][k] < INF)  // iterowanie po kolumnach, czyli w poziomie
                found_vector.push_back(static_cast<bool>(cost_matrix_[t][k]));
        }
        if(found_vector.size()) {
            found = (std::find(found_vector.begin(), found_vector.end(), false) == found_vector.end());
            if (found)
                return true;
        }
    }
    bool found_2;
    std::vector<bool> found_vector_2;
    for(int l = 0; l < num_of_summits_; ++l){
        found_vector_2.clear();
        for(int u = 0; u < num_of_summits_; ++u){
            if(cost_matrix_[u][l] < INF)  // iterowanie po wierszach, czyli w pionie
                found_vector_2.push_back(static_cast<bool>(cost_matrix_[u][l]));

        }
        if(found_vector_2.size()) {
            found_2 = (std::find(found_vector_2.begin(), found_vector_2.end(), false) == found_vector_2.end());
            if (found_2)
                return true;
        }
    }
    return false;


}
void TSP_cost_matrix::reduce_row(int row){
    the_lowest_ = INF;
    for(int r = 0; r < num_of_summits_; ++r){
        if(cost_matrix_[row][r] == INF)
            continue;
        if(cost_matrix_[row][r] < the_lowest_)
            the_lowest_= cost_matrix_[row][r];
    }
    if(the_lowest_ != INF) {
        vector_low_bound_.push_back(the_lowest_);
        low_bound_ += the_lowest_;
    }
    else{
        vector_low_bound_.push_back(0);
    }
}
void TSP_cost_matrix::reduce_all_rows() {
    if (found_zero()) {
        for (int i = 0; i < num_of_summits_; ++i) {
            reduce_row(i);
        }
        int c = 0;
        for (auto& z : cost_matrix_) {
            for (int ind = 0; ind < num_of_summits_; ++ind) {
                if (z[ind] == INF)
                    continue;
                z[ind] -= vector_low_bound_[c];
            }
            ++c;
        }
        vector_low_bound_.clear();
    }
}
void TSP_cost_matrix::reduce_col(int col){    //zmień inta na coś innego
    the_lowest_= INF;
    for(int c = 0; c < num_of_summits_; ++c){
        if(cost_matrix_[c][col] == INF)
            continue;
        if(cost_matrix_[c][col] < the_lowest_)
            the_lowest_ = cost_matrix_[c][col];
    }
    if(the_lowest_ != INF) {
        vector_low_bound_.push_back(the_lowest_);
        low_bound_ += the_lowest_;
    }
    else
        vector_low_bound_.push_back(0);
}
void TSP_cost_matrix::reduce_all_cols() {

    if (found_zero()) {
        for (int i = 0; i < num_of_summits_; ++i) {
            reduce_col(i);
        }
        for (auto& z : cost_matrix_) {
            for (int ind = 0; ind < num_of_summits_; ++ind) {
                if (z[ind] == INF)
                    continue;
                z[ind] -= vector_low_bound_[ind];
            }
        }
        vector_low_bound_.clear();
    }
}
int TSP_cost_matrix::get_path_cost(int row_num, int col_num){
    return static_cast<int>(cost_matrix_[row_num][col_num]);
}
void TSP_cost_matrix::find_next_path(){
    std::vector<int> row_col;
    std::vector<int> rows_and_cols;
    while(static_cast<int>(sol_.size()) < num_of_summits_){
        int counter = 0;
        vec_of_zer_.clear();
        for(int rows = 0; rows < num_of_summits_; ++rows){
            for(int cols = 0; cols < num_of_summits_; ++cols){
                if(0 == cost_matrix_[rows][cols]){
                    row_col.clear();
                    row_col.push_back(rows);
                    row_col.push_back(cols);
                    vec_of_zer_.push_back(row_col);
                    ++counter;
                }
            }
        }
        for(int e = 0; e < counter; ++e){
            int lowest_row = UINF;
            int lowest_col = UINF;
            int sum_of_lowest = 0;
            for(int x = 0; x < num_of_summits_; ++x){
                if(x != vec_of_zer_[e][1] && (cost_matrix_[vec_of_zer_[e][0]][x] < lowest_row) ){
                    lowest_row = get_path_cost(vec_of_zer_[e][0], x);
                }
                if(x!=vec_of_zer_[e][0] && (cost_matrix_[x][vec_of_zer_[e][1]])< lowest_col){
                    lowest_col = get_path_cost(x, vec_of_zer_[e][1]);
                }
            }
            if (UINF != lowest_col && UINF != lowest_row) {
                sum_of_lowest = lowest_col + lowest_row;
                vec_of_zer_[e].push_back(sum_of_lowest);
            }
        }
        int biggest_sum = -1;
        rows_and_cols.clear();
        for(int inx = 0; inx < counter; ++inx){
            if(static_cast<int>(vec_of_zer_[inx][2]) > biggest_sum) {
                biggest_sum = vec_of_zer_[inx][2];
                rows_and_cols = vec_of_zer_[inx];
            }
        }
        solution_.clear();
        solution_.push_back(static_cast<int>(rows_and_cols[0]));
        solution_.push_back(static_cast<int>(rows_and_cols[1]));
        bool flag_of_last = false;
        int number_of_INF = 0;
        if(static_cast<int>(sol_.size()) == (num_of_summits_-2)){
            for(int y=0; y < num_of_summits_; ++y) {
                number_of_INF = 0;
                for (int r = 0; r < num_of_summits_; ++r) {
                    if (!flag_of_last && cost_matrix_[y][r] == INF) {
                        ++number_of_INF;
                    }

                }
                if (number_of_INF == (num_of_summits_ - 1) || flag_of_last) {
                    flag_of_last = true;
                    for (int p = 0; p < num_of_summits_; ++p) {
                        if (cost_matrix_[y][p] != INF) {
                            solve_all_.push_back(std::make_pair(y + 1, p + 1));
                            sol_.push_back(y);                                 //Tu też !!!!!!
                            for (int q = 0; q < num_of_summits_; ++q) {
                                cost_matrix_[y][q] = INF;
                                cost_matrix_[q][p] = INF;
                            }
                            cost_matrix_[p][y] = INF;
                            y = 0;
                        }
                    }
                }
            }
        } else {
            solve_all_.push_back(std::make_pair(solution_[0] + 1, solution_[1] + 1));
            sol_.push_back(static_cast<int>(rows_and_cols[0]));
            for (int q = 0; q < num_of_summits_; ++q) {
                cost_matrix_[static_cast<int>(rows_and_cols[0])][q] = INF;
                cost_matrix_[q][static_cast<int>(rows_and_cols[1])] = INF;
            }
            cost_matrix_[static_cast<int>(rows_and_cols[1])][static_cast<int>(rows_and_cols[0])] = INF;
            cost_matrix_[static_cast<int>(rows_and_cols[0])][static_cast<int>(rows_and_cols[1])] = INF;
        }
        reduce_all_rows();
        reduce_all_cols();
    }
    the_final_solution__.push_back(solve_all_[0].first);
    the_final_solution__.push_back(solve_all_[0].second);
    for(int a = 0; the_final_solution__.size() <= solve_all_.size(); ++a){
        auto numer = solve_all_[a].first;
        if(the_final_solution__[the_final_solution__.size()-1] == numer) {
            the_final_solution__.push_back(solve_all_[a].second);
            a = 0;
        }
    }
}
