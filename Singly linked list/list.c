#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define ROZMIAR 30

typedef struct base{

    char imie[ROZMIAR];
    char nazwisko[ROZMIAR];
    int rok;

}base;

typedef struct list{

    struct base data;
    struct list *next;
}list;

void clear_buffer() {
    while (getchar() != '\n');
}

void printList(struct list *head){
    list *tmp;
    if(!head){
        puts("Lista nie zawiera żadnych wpisów!");
        return;
    }
    int i=1;
    puts("Lista zawiera następujące dane:");
    for(tmp=head; tmp!=NULL; tmp=tmp->next){
        printf("\nPozycja nr %d:\n\n",i++);
        printf("%s",tmp->data.imie);
        printf("%s",tmp->data.nazwisko);
        printf("%d\n",tmp->data.rok);
}
}
struct list * addList(struct list *head) {

    struct list *tmp;
    tmp = (struct list *) malloc(sizeof(struct list));
    if (tmp == NULL)
        return head;
    if (head == NULL){
        tmp->next = NULL;
        head = tmp;
    }
    else {
        tmp->next=head;
    }
    puts("Podaj imię osoby, którą chcesz dodać:");
    fgets(tmp->data.imie,ROZMIAR,stdin);
    puts("Podaj nazwisko:");
    fgets(tmp->data.nazwisko,ROZMIAR,stdin);
    puts("Wpisz rok urodzenia:");
    scanf("%d",&tmp->data.rok);
    clear_buffer();
    head=tmp;
    return head;
}

void deleteElem(struct list **head){
    struct list *tmp;
    if(head==NULL){
        puts("Lista jest pusta - niczego nie usunięto!");
        return;
    }
    tmp=*head;
    *head=tmp->next;
    free(tmp);
    puts("Element usunięto!");
}

void deleteList(struct list **head){
    struct list *tmp;
    if(head==NULL){
        puts("Lista jest pusta - niczego nie usunięto!");
        return;
    }
    for(tmp=*head;tmp!=NULL;tmp=*head) {
        *head = tmp->next;
        free(tmp);
        }
    puts("Lista wyczyszczona!");
}

int main () {
    struct list *head;
    head = NULL;
    int operacja = 0;
    do {
        printf("\nCo mam zrobić:\n");
        puts("0 - wypisanie zawartosci listy");
        puts("1 - dodanie danych do listy");
        puts("2 - usuwanie elementu z początku listy");
        puts("3 - usuwanie całej listy");
        puts("4 - wyjście z programu");
        scanf("%d", &operacja);
        clear_buffer();
        switch (operacja) {

            case 0:
                printList(head);
                continue;
            case 1:
                head = addList(head);
                continue;
            case 2:
                deleteElem(&head);
                continue;
            case 3:
                deleteList(&head);
                continue;
        }
    } while (operacja != 4);
    return 0;
}