# <302936> <Adrian> <Winkler>

from typing import List
from typing import Dict

def adjmat_to_adjlist(adjmat: List[List[int]]) -> Dict[int, List[int]]:
    adjlist = {}
    list = []
    for i in range(len(adjmat)):
        for j in range(len(adjmat[i])):
            if adjmat[i][j] != 0:
                for x in range(adjmat[i][j]):
                    list.append(j+1)
        if list:
            adjlist[i+1] = list
            list = []
    return adjlist

def dfs_re(G: Dict[int, List[int]], s: int, visited) ->List[int]:
    s_tmp = G[s]
    for index in range(len(s_tmp)):
        if s_tmp[index] not in visited:
            visited.append(s_tmp[index])
            dfs_re(G, s_tmp[index], visited)
    return visited


def dfs_recursive(G: Dict[int, List[int]], s: int) -> List[int]:
    visited = [s]
    s_tmp = G[s]
    for index in range(len(s_tmp)):
        if s_tmp[index] not in visited:
            visited.append(s_tmp[index])
            dfs_re(G, s_tmp[index], visited)
    return visited


def dfs_iterative(G: Dict[int, List[int]], s: int) -> List[int]:
    stack = [s]
    visited = []
    v = [s]
    while stack:
        v = stack.pop(-1)
        if v not in visited:
            visited.append(v)
            length = len(G[v])
            list_neighbour = G[v]
            for index in range(length):
                stack.append(list_neighbour[length - index-1])
    return visited





def is_acyclic(G: Dict[int, List[int]]) -> bool:
   for index in G:
       visited = []
       list = []
       stack = [index]
       while stack:
           v = stack[-1]
           stack.pop()
           if v not in visited:
               visited += [v]
               for neighbour in G[v]:
                   list.append(neighbour)
               list.reverse()
               stack += list
               list = []
               if index in stack:
                   return False
   return True
