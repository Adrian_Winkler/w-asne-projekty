# <Adrian> <Winkler> <302936>

import unittest
import networkx as nx


def make_graph():
    va = [1, 2, 2, 1]
    vb = [2, 3, 3, 3]
    w = [0.5, 0.4, 0.3, 1.0]
    graphs = nx.MultiDiGraph()
    for inx in range(len(va)):
        graphs.add_edges_from([(va[inx], vb[inx], dict(weight = w[inx]))])
    return graphs

        
class TestFunctions(unittest.TestCase):       
    def test_dijkstra_path(self):
        graph = make_graph()
        self.assertEqual(nx.dijkstra_path_length(graph, 1, 3), 0.8)
        self.assertEqual(nx.dijkstra_path_length(graph, 1, 2), 0.5)
        self.assertEqual(nx.dijkstra_path_length(graph, 2, 3), 0.3)


if __name__ == '__main__':
    unittest.main()
