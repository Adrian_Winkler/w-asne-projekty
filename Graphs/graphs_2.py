# <Adrian> <Winkler> <302936>

#$$$$$$$$$$$$$$$$$$$$$$$$$$$ - Dijkstra Path - $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

import networkx as nx
from typing import List, NamedTuple, Dict, Set
import matplotlib.pyplot as plt
from enum import Enum, auto


VertexID = int
EdgeID = int


class TrailSegmentEntry(NamedTuple):
    
    v_start : VertexID
    v_end : VertexID
    edge_id : EdgeID
    edge_weight : float
    

Trail = List[TrailSegmentEntry]

def load_multigraph_from_file(filepath):
    file = open(filepath) 
    data = file.read()  
    string = data.split() 
    va = [int(string[x]) for x in range(len(string)) if x%3 == 0]
    vb = [int(string[x]) for x in range(len(string)) if x%3 == 1]
    w = [float(string[x]) for x in range(len(string)) if x%3 == 2]
    g = nx.MultiDiGraph()
    for inx in range(len(va)):
        g.add_edges_from([(va[inx], vb[inx], dict(weight = w[inx]))])
    
    return g

def find_min_trail(g: nx.MultiDiGraph, v_start: VertexID, v_end: VertexID) -> Trail:

    list_of_vertex = []
    trail:Trail = []
    dijkstra_path_nodes= nx.dijkstra_path(g, v_start, v_end) 
    for i in range(len(dijkstra_path_nodes)-1): 
        for iden in g[dijkstra_path_nodes[i]][dijkstra_path_nodes[i+1]]: 
            list_of_vertex.append(g[dijkstra_path_nodes[i]][dijkstra_path_nodes[i+1]][iden]['weight'])
        min_w=min(list_of_vertex)
        trail.append(TrailSegmentEntry(dijkstra_path_nodes[i],dijkstra_path_nodes[i+1],list_of_vertex.index(min_w),min_w)) #jako, ze for przechodzi po kolei po identyfikatorach
        list_of_vertex.clear()
        
    return trail

 
def trail_to_str(trail: Trail) -> str:

    trail_string = ""
    total : int = 0 
    for elem in trail:
        trail_string += str(elem.v_start) + " -["
        trail_string += str(elem.edge_id) + ": "
        trail_string += str(elem.edge_weight) + "]-> "
        total += elem.edge_weight
        
    trail_string += str(trail[-1].v_end) + "  (total = "
    trail_string += str(total) + ")"
    
    return trail_string 


#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ - BFS - $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

VertexID = int
AdjList = Dict[VertexID, List[VertexID]]
Distance = int

class Color(Enum):
    WHITE = auto()
    GREY = auto()
    BLACK = auto()


def add_to_keys(G: AdjList):
    list_of_value = []
    for value in G.values():
        list_of_value.extend(value)
    for key in list_of_value:
        if key not in G.keys():
            G[key] = []
    return G

def neighbors(adjlist: AdjList, start_vertex_id: VertexID,
                  max_distance: Distance) -> Set[VertexID]:
    adjlist = add_to_keys(adjlist)
    queqe = [] #lista
    color = {} #słownik, wyrazy się nie powtarzają
    distance = 0
    sets = (start_vertex_id, distance)
    for u in adjlist.keys():
        color[u] = Color.WHITE
    color[start_vertex_id] = Color.GREY
    queqe.append(sets)
    container = set()
    while queqe:
        u = queqe.pop(0)
        vertices = u[0]
        distance = u[-1]+1
        if u[-1] >= max_distance:
            return container
        for v in adjlist[vertices]:
            if color[v] == Color.WHITE:
                container.add(v)
                color[v] = Color.GREY
                sets = (v, distance)
                queqe.append(sets)
        color[vertices] = Color.BLACK
    return container







