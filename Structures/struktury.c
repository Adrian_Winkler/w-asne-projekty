#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define DLUG 30
#define SIZE 10

typedef struct wpis{
    char imie[DLUG];
    char nazwisko[DLUG];
    long numer;
}database;

typedef struct baza {
    int max_size;
    int act_size;
    database *tab;
}baza;

void wypisz (baza *b){

    printf("\n\nMaksymalna ilość rekordów  możliwych do zapisania: %d\n",b->max_size);
    printf("Obecna ilość rekordów  zapisanych w bazie: %d\n",b->act_size);
    puts("Baza zawiera następujące elementy:");

    for (int i = 0; i <b->act_size ; ++i) {
        printf("\nKontakt numer %d:\n",(i+1));
        printf("%s", b->tab[i].imie);
        printf("%s", b->tab[i].nazwisko);
        printf("%li\n\n", b->tab[i].numer);
    }
}

int dodaj( struct baza *a_bazy, struct wpis *a_wpisu)
{
    int k=a_bazy->act_size;
    if (k==a_bazy->max_size) {
       a_bazy->tab = realloc(a_bazy->tab, ((a_bazy->max_size + 10) * sizeof(database)));
       a_bazy->max_size+=10;
    }
    if(a_bazy->tab==NULL)
        return -1;
    for (int i = 0; a_wpisu->imie[i]!='\0' ; ++i)
      a_bazy->tab[k].imie[i] = a_wpisu->imie[i];
    for (int j = 0; a_wpisu->nazwisko[j]!='\0' ; ++j)
        a_bazy->tab[k].nazwisko[j] = a_wpisu->nazwisko[j];
        a_bazy->tab[k].numer =a_wpisu->numer;
}

struct baza usun( struct baza par_baza, int indeks) {

    for (int i = indeks; i < par_baza.act_size; ++i) {
        par_baza.tab[i] = par_baza.tab[i + 1];
        --par_baza.act_size;
        return par_baza;
    }
}
void buffor_clear()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF) {};
}

int main () {

    int indeks = 0, error = 0;
    struct baza b;
    b.max_size = SIZE;
    b.act_size = 0;
    database *tab;
    b.tab =(database*) malloc(SIZE * sizeof(database));
    struct wpis n_kontakt;
    int operacja = 0;
    do {
        printf("Co mam zrobić? (1 - wypisanie, 2 - dodawanie, 3 - usuwanie, 0 - koniec)\n");
        scanf("%d", &operacja);
        buffor_clear();

        switch (operacja) {
            case 1:
                wypisz(&b);
                continue;
            case 2:
                puts("\nPodaj imię osoby, którą chcesz dodać:");
                fgets(n_kontakt.imie, DLUG, stdin);
                puts("\nPodaj nazwisko osoby, którą chcesz dodać:");
                fgets(n_kontakt.nazwisko, DLUG, stdin);
                puts("\nPodaj numer telefonu osoby, którą chcesz dodać:");
                scanf("%li", &n_kontakt.numer);
                buffor_clear();
                error=dodaj(&b, &n_kontakt);
                ++b.act_size;
                if (error == -1)
                {
                    puts("Błąd alokacji pamięci!");
                    return -1;
                }
                continue;
            case 3:
                printf("Który element bazy mam usunąć?: 1 - %d\n", (b.act_size));
                scanf("%d", &indeks);
                b = usun(b, (indeks-1));
                continue;
        }
    } while (operacja != 0);
    free(b.tab);

}
