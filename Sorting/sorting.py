# Adrian Winkler 302936
import random
from typing import List
from timeit import timeit

#quicksort:
def quicksort_copy(list: List, start: int, stop: int):
    i = start
    j = stop
    pivot = (start + stop)//2
    while i < j:
        while list[i] < list[pivot]:
            i = i + 1
        while list[j] > list[pivot]:
            j = j - 1
        if i <= j:
            tmp = list[i]
            list[i] = list[j]
            list[j] = tmp
            i = i + 1
            j = j - 1
    if start < j:
        quicksort_copy(list, start, j)
    if i < stop:
        quicksort_copy(list, i, stop)


def quicksort(list: List):
    list_copy = list[:]
    quicksort_copy(list_copy, 0, len(list_copy) - 1)
    #print("\nQuicksort:\n")
    #print(list_copy)



#bubblesort:
def bubblesort_copy(list_copy_2) -> tuple:
    n = len(list_copy_2)
    counter = 0
    while n > 1:
        flag = False
        for i in range(1,n):
            if list_copy_2[i-1] > list_copy_2[i]:
                tmp = list_copy_2[i]
                list_copy_2[i] = list_copy_2[i - 1]
                list_copy_2[i - 1] = tmp
                flag = True

        counter += 1
        n -= 1
        if not flag:
            break
    tuple = (list_copy_2, counter)
    return tuple
def bubblesort(list: List):
    list_copy_2 = list[:]
    tpl = bubblesort_copy(list_copy_2)
    #print("\nBubblesort:\n")
    #print(list_copy_2)
    #print(tpl[1])
    return tpl
#list = random.sample(range(1, 2233), 10)
#print(list)
#quicksort(list)
#bubblesort(list)

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

# Tests

sum_time_quicksort = 0
sum_time_bubblesort = 0
for i in range(5):
    list_random = random.sample(range(1, 2233), 100)
    time_quicksort = timeit("quicksort(list_random)", number=1, globals=globals())
    time_bubblesort = timeit("bubblesort(list_random)", number=1, globals=globals())
    sum_time_quicksort += time_quicksort
    sum_time_bubblesort += time_bubblesort
sum_time_quicksort /= 5
sum_time_bubblesort /= 5
print("\nRandom list:")
print("Quicksort: %.7f" % sum_time_quicksort)
print("Bubblesort: %.7f " % sum_time_bubblesort, "\n")



sum_time_quicksort = 0
sum_time_bubblesort = 0
list_sorted = [element for element in range(1, 101, 1)]
time_quicksort = timeit("quicksort(list_sorted)", number=1, globals=globals())
time_bubblesort = timeit("bubblesort(list_sorted)", number=1, globals=globals())
sum_time_quicksort += time_quicksort
sum_time_bubblesort += time_bubblesort
print("Sorted list:")
print("Quicksort: %.7f" % sum_time_quicksort)
print("Bubblesort: %.7f\n" % sum_time_bubblesort)



sum_time_quicksort = 0
sum_time_bubblesort = 0
list_reversed = [101 - element for element in range(1, 101, 1)]
time_quicksort = timeit("quicksort(list_reversed)", number=1, globals=globals())
time_bubblesort = timeit("bubblesort(list_reversed)", number=1, globals=globals())
sum_time_quicksort += time_quicksort
sum_time_bubblesort += time_bubblesort
print("Reversed list:")
print("Quicksort: %.7f" % sum_time_quicksort)
print("Bubblesort: %.7f\n" % sum_time_bubblesort)



sum_time_quicksort = 0
sum_time_bubblesort = 0
list_same_ele = [0]*100
time_quicksort = timeit("quicksort(list_same_ele)", number =1, globals = globals())
time_bubblesort = timeit("bubblesort(list_same_ele)", number = 1, globals = globals())
sum_time_quicksort += time_quicksort
sum_time_bubblesort += time_bubblesort
print("The same elements:")
print("Quick sort: %1f" % sum_time_quicksort)
print("Bubblesort: %1f\n" % sum_time_bubblesort)



