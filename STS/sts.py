from selenium import webdriver
import requests
import bs4
from datetime import datetime
import xlsxwriter
import copy
import re
import openpyxl
import time
from PIL import Image
from io import BytesIO
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
import PIL

def sts_data_collect():
    flag = True
    # book = openpyxl.load_workbook('hello.xlsx')
    # # workbook = xlsxwriter.Workbook('probe.xlsx')
    # # worksheet = workbook.add_worksheet()
    # sheet = book.active
    # max = sheet.max_row
    flag_2 = int(input("How long program should work?:\n"))
    flag_3 = int(input('Enter the time how often data should be updated - in minutes?\n'))
    flag_3 *= 60
    counter_of_image = 2
    counter = 0
    while flag:
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print("Current loop started at: ", current_time)
        book = openpyxl.load_workbook('probe.xlsx')
        # workbook = xlsxwriter.Workbook('probe.xlsx')
        # worksheet = workbook.add_worksheet()
        sheet = book.active
        response = requests.get('https://www.sts.pl')
        vector_of_number_bet = []
        vector_of_date = []
        vector_of_bet = []
        vector_of_match = []
        vector_of_type = []
        vector_of_curse = []
        vector_of_bet_version = []
        vector_of_bid = []
        vector_of_nr_bet = []
        vector_of_price_win = []
        vector_of_single_curse = []
        # worksheet.write('A1', 'Dane')
        maxsize = (sheet.max_row + 2)
        maxsize_str = str(maxsize)
        # print(maxsize)
        a_column = 'A'+maxsize_str
        b_column = 'B'+maxsize_str
        c_column = 'C'+maxsize_str
        d_column = 'D'+maxsize_str
        e_column = 'E' + maxsize_str
        f_column = 'F' + maxsize_str
        g_column = 'G' + maxsize_str
        h_column = 'H' + maxsize_str
        i_column = 'I' + maxsize_str
        j_column = 'J' + maxsize_str
        k_column = 'K' + maxsize_str
        ctr =0
        vector_of_wins = []
        vector_of_winners = []
        vector_of_money = []
        vector_of_real_curse = []
        bs = bs4.BeautifulSoup(response.text, "html5lib")
        for name_win in bs.find_all("span", class_="win-box-name"):
            vector_of_wins.append(name_win.get_text())

        for pdata in bs.find_all("div", "lift-win-holder"):
            onclick_str = pdata.get("onclick")
            # worksheet.write(b_column, str(vector_of_wins[ctr]))
            # b_column = 'B' + str(maxsize)
            onclick_str_cupon_number = onclick_str[24:42]
            link_to_coupon = 'https://www.sts.pl/pl/user/?action=show-ticket-detail&id=' + onclick_str_cupon_number + '&live=&ajaxCall=1&ticket_index=SOLO&fullticket=1'
            vector_of_line = []
            data_cupon = requests.get(link_to_coupon)
            bs_2 = bs4.BeautifulSoup(data_cupon.text, "html5lib")
            for cupon_win in bs_2.find_all("div", class_="wygrana"):
                tmp_string = cupon_win.get_text()
                # print(tmp_string)
                tmp_str = re.findall(r'\d{4}\.\d{2}', tmp_string)
                # tmp_str = tmp_string.split()
                vector_of_price_win.append(tmp_str[0])
                # print(tmp_str[0])
            for cupon_kurs in bs_2.find_all("td", class_="kurs"):
                vector_of_curse.append(cupon_kurs.get_text())
            for cupon_typ in bs_2.find_all("td", class_="typ"):
                vector_of_type.append(cupon_typ.get_text())
            for cupon_single_curse in bs_2.find_all('td', id=False, class_=False, text=re.compile('\A\d\.\d\d')):
                if cupon_single_curse.get_text()!='0.88':
                    vector_of_single_curse.append(cupon_single_curse.get_text())
            for cupon_nr in bs_2.find_all("td", class_="nr"):
                 vector_of_nr_bet.append(cupon_nr.get_text())
                 vector_of_winners.append(vector_of_wins[ctr])
                 vector_of_money.append(vector_of_price_win[ctr])
                 if ctr <= (len(vector_of_curse)-1):
                    vector_of_real_curse.append(vector_of_curse[ctr])
            for cupon_date in bs_2.find_all("td", class_="date_time"):
                 vector_of_date.append(cupon_date.get_text())
            for cupon_bet in bs_2.find_all("td", class_="typ"):
                 vector_of_bet.append(cupon_bet.get_text())
            # for cupon_match in bs_2.find_all("span", class_="mecz"):
            #      vector_of_match.append(cupon_match.get_text())
            for coupon_match_more in bs_2.find_all('th', class_=False, id=False, ):
                info = coupon_match_more.get_text()
                split_info = info.split('\n')
                if len(split_info[0])>23:
                    vector_of_match.append(split_info[0])
                # print(split_info)
                # print(cupon_match_more.get_text())
            for coupon_result in bs_2.find_all("th", class_="title"):
                vector_of_line.append(copy.deepcopy(coupon_result.get_text()))

            bet_version = copy.deepcopy(vector_of_line[0])
            nr_coupon = copy.deepcopy(vector_of_line[2])
            bid = copy.deepcopy(vector_of_line[-1])
            bet_version = re.findall(r'SOLO|AKO|MAXIKOMBI', bet_version)
            nr_coupon = re.findall(r'\d{3} \d{3} \d{3} \d{3} \d{3} \d{3}', nr_coupon)
            bid = re.findall(r'\d{1,5}.\d{2}', bid)
            for coupon_result in bs_2.find_all("span", class_="mecz"):
                if len(bet_version)!= 0:
                    vector_of_bet_version.append(bet_version[0])
                vector_of_number_bet.append(nr_coupon[0])
                vector_of_bid.append(bid[0])
            ctr += 1
        course_total = 1
        first_i = 0
        i_counter = 0
        flag_of_first = False
        for i in range(len(vector_of_bet_version)):
            if vector_of_bet_version[i] == 'MAXIKOMBI':
                course_total *= float(vector_of_single_curse[i])
                i_counter += 1
                if not flag_of_first:
                    flag_of_first = True
                    first_i = i
        for h in range(i_counter):
            vector_of_real_curse.insert(first_i, course_total)
            first_i += 1
        string_of_curse = ''
        vector_of_course = []
        for i in vector_of_single_curse:
            string_of_curse += i
        string_date = re.findall(r'[1-9]\.\d\d', vector_of_date[0])
        vector_of_course = re.findall(r'[1-9]\.\d\d', string_of_curse)
        for elem in vector_of_course:
            if elem == string_date[0]:
                vector_of_course.remove(elem)
        print("Długość vektorów: ")
        print("Vektor Nr: ", len(vector_of_nr_bet))
        print("Vektor Autorów: ", len(vector_of_winners))
        print("Vektor Daty: ", len(vector_of_date))
        print("Vektor Meczu: ", len(vector_of_match))
        print("Vektor Typu Zakładu: ", len(vector_of_bet_version))
        print("Vektor Stawki: ", len(vector_of_bid))
        print("Vektor Typu: ", len(vector_of_type))
        print("Vektor Wygranej: ", len(vector_of_money))
        print("Vektor Długiego Numeru: ", len(vector_of_number_bet))
        print("Vektor Kursu: ", len(vector_of_real_curse))
        print("Vektor Kursów Pojedyńczych: ", len(vector_of_course), '\n')

        for i in range(len(vector_of_number_bet)):
            flag_of_being = True
            for row in range(1, sheet.max_row + 1):
                cell_name_d = "D{}".format(row)
                cell_name_k = "K{}".format(row)
                # print(sheet[cell_name_d].value, '\n')
                # print(len(str(sheet[cell_name_d].value)))
                # print(len(vector_of_match[i]), '\n')
                # print(sheet[cell_name_k].value)

                if (sheet[cell_name_d].value == str(vector_of_match[i])) and (sheet[cell_name_k].value == str(vector_of_number_bet[i])):
                    print('Yes')
                    flag_of_being = False
                    break
            if flag_of_being:
                sheet[a_column] = vector_of_nr_bet[i]
                sheet[b_column] = vector_of_winners[i]
                sheet[c_column] = vector_of_date[i]
                sheet[d_column] = vector_of_match[i]
                if i < len(vector_of_bet_version):
                    sheet[e_column] = str(vector_of_bet_version[i])
                if i < len(vector_of_real_curse):
                    sheet[f_column] = float(vector_of_real_curse[i])
                sheet[g_column] = vector_of_type[i]
                sheet[i_column] = str(vector_of_bid[i])
                sheet[j_column] = vector_of_money[i]
                sheet[k_column] = str(vector_of_number_bet[i])
                if i<len(vector_of_course):
                    sheet[h_column] = float(vector_of_course[i])

                maxsize += 1
                maxsize_str = str(maxsize)
                a_column = 'A' + maxsize_str
                b_column = 'B' + maxsize_str
                c_column = 'C' + maxsize_str
                d_column = 'D' + maxsize_str
                e_column = 'E' + maxsize_str
                f_column = 'F' + maxsize_str
                g_column = 'G' + maxsize_str
                h_column = 'H' + maxsize_str
                i_column = 'I' + maxsize_str
                j_column = 'J' + maxsize_str
                k_column = 'K' + maxsize_str

        # name = 'screenshot' + str(counter) + '.png'
        # counter += 1
        # driver = webdriver.Chrome(executable_path=r'C:\Users\WINKLER\Desktop\Wesele\chromedriver_win32\chromedriver.exe')
        # driver.get('https://vgls.betradar.com/s4/?clientid=2842&language=pl&vsportid=1#2_1,3_1111,22_7,5_1765514,9_statistics,231_overunder,30_1,237_1.5')
        # time.sleep(2)
        # png = driver.get_screenshot_as_png()
        # im = Image.open(BytesIO(png))
        # im.save(name)
        # img = Image.open(name)
        # w, h = img.size
        # img.crop((0, 270, w, h - 290)).save(name)
        # screenshot = openpyxl.drawing.image.Image(name)
        # counter_of_image += 18
        # cell_image = 'N' + str(counter_of_image)
        # sheet.add_image(screenshot, cell_image)
        # book.save('probe.xlsx')
        # driver.close()
        book.save('probe.xlsx')
        flag_2 -= 1
        time.sleep(flag_3)
        if flag_2 <= 0:
            quit()
        time.sleep(flag_3)
    book.save('probe.xlsx')


def fun_screenshot():
    book = openpyxl.Workbook()
    sheet = book.worksheets[0]
    counter = 0
    name = 'screenshot' + str(counter)+'.png'
    counter += 1
    max = sheet.max_row
    driver = webdriver.Chrome(executable_path=r'C:\Users\WINKLER\Desktop\Wesele\chromedriver_win32\chromedriver.exe')
    driver.get('https://www.sts.pl/pl/sporty-wirtualne/#/')
    time.sleep(5)

    # browser.execute_script("window.open('');")
    # Switch to the new window and open URL B

    stats = driver.find_element_by_class_name('stats')
    print(stats)
    stats.click()
    time.sleep(4)
    driver.switch_to.window(driver.window_handles[1])
    print(driver.current_url)
    statistics = driver.find_element_by_link_text('Statystyki')
    statistics.click()
    time.sleep(5)

    # statistic = driver.get('https://vgls.betradar.com/s4/?clientid=2842&language=pl&vsportid=1#2_1,3_1111,22_7,5_1765514,9_statistics,231_overunder,30_1,237_1.5')
    # elem = driver.find_elements_by_id('loader')
    # print(elem)
    # location = elem.location
    # size = elem.size
    time.sleep(3)
    png = driver.get_screenshot_as_png()
    im = Image.open(BytesIO(png))
    im.save(name)
    img = Image.open(name)

    w, h = img.size
    img.crop((0, 170, w, h - 220)).save(name)
    screenshot = openpyxl.drawing.image.Image(name)
    sheet.add_image(screenshot, 'A1')
    # print(sheet.max_row)

    name = 'screenshot' + str(counter)+'.png'
    more_less = driver.find_element_by_link_text('Ponad/Poniżej')
    more_less.click()
    time.sleep(2)
    one_five = driver.find_element_by_link_text('1.5')
    one_five.click()
    time.sleep(2)

    png = driver.get_screenshot_as_png()
    im = Image.open(BytesIO(png))
    im.save(name)
    img = Image.open(name)

    w, h = img.size
    img.crop((0, 270, w, h - 100)).save(name)
    screenshot = openpyxl.drawing.image.Image(name)
    sheet.add_image(screenshot, 'A18')

    book.save('probe.xlsx')
    driver.close()

    # wb = openpyxl.Workbook()
    # ws = wb.worksheets[0]
    # img = openpyxl.drawing.image.Image('test.jpg')
    # img.anchor = 'A1'
    # ws.add_image(img)
    # wb.save('out.xlsx')

    return True


def fun_avarage():
    book = openpyxl.Workbook()
    sheet = book.worksheets[0]
    max = sheet.max_row
    driver = webdriver.Chrome(executable_path=r'C:\Users\WINKLER\Desktop\Wesele\chromedriver_win32\chromedriver.exe')
    driver.get('https://www.sts.pl/pl/sporty-wirtualne/#/')
    time.sleep(5)
    stats = driver.find_element_by_class_name('stats')
    stats.click()
    time.sleep(4)
    driver.switch_to.window(driver.window_handles[1])
    agenda = driver.find_element_by_link_text('agenda')
    agenda.click()
    # print(driver.current_url)
    # script_of_side = requests.get(driver.current_url)
    # print(script_of_side.text)
    # bs_2 = bs4.BeautifulSoup(script_of_side.text, "html5lib")
    # for sezon in bs_2.find_all(text='Sezon'):
    #     print(sezon)
    # sezon = driver.find_element_by_xpath('//*[@id="node_main-wrap"]/div[5]/div[1]/div/div/div[2]/div/div/div[1]/div[1]/div[1]/div/div/div[1]/div/div[1]')
    # print(sezon.get_attribute("text"))
    time.sleep(3)
    # book.save('statistic.xlsx')

    arrow = driver.find_element_by_class_name('arrow')
    arrow.click()
    time.sleep(4)
    choose = driver.find_element_by_xpath('//*[@id="node_main-wrap"]/div[5]/div[1]/div/div/div[2]/div/div/div[1]/div[1]/div[1]/div/div/div[1]/div/div[1]').text
    print(choose)
    print(choose[-5:])
    season_number = int(choose[-5:])
    book = openpyxl.load_workbook('probe.xlsx')
    sheet = book.active
    max = sheet.max_row
    for i in range(7):
        season_number -= 1
        str_season = 'Sezon '+ str(season_number)
        new_season = driver.find_elements_by_class_name('sb-option')
        new_season[i].click()
        time.sleep(3)
        # bs_2 = bs4.BeautifulSoup(script_of_side.text, "html5lib")
        item = driver.find_elements_by_class_name('ftx')
        pointer_on_round = 0
        sum_of_miss = 0
        for ind in item:
            pointer_on_round += 1
            str_wynik = ind.text
            print(str_wynik)
            if len(str_wynik) == 0:
                break
            wynik = int(str_wynik[0]) + int(str_wynik[-1])
            if wynik < 3:
                sum_of_miss += 1
            if pointer_on_round%8 == 0:
                if max%31==0:
                    max += 1
                str_cell_a = 'A' + str(max)
                str_cell_b = 'B' + str(max)
                str_cell_c = 'C' + str(max)
                sheet[str_cell_a] = 'Runda ' + str(max%31)
                sheet[str_cell_b] = 8 - sum_of_miss
                sheet[str_cell_c] = sum_of_miss
                sum_of_miss = 0
                max += 1
        arrow = driver.find_element_by_class_name('arrow')
        arrow.click()
        time.sleep(3)

    book.save('probe.xlsx')
    driver.close()
    return True



def fun_draw():
    book = openpyxl.Workbook()
    sheet = book.worksheets[0]
    max = sheet.max_row
    driver = webdriver.Chrome(executable_path=r'C:\Users\WINKLER\Desktop\Wesele\chromedriver_win32\chromedriver.exe')
    driver.get('https://www.sts.pl/pl/sporty-wirtualne/#/')
    time.sleep(5)
    stats = driver.find_element_by_class_name('stats')
    stats.click()
    time.sleep(4)
    driver.switch_to.window(driver.window_handles[1])
    agenda = driver.find_element_by_link_text('agenda')
    agenda.click()
    # print(driver.current_url)
    # script_of_side = requests.get(driver.current_url)
    # print(script_of_side.text)
    # bs_2 = bs4.BeautifulSoup(script_of_side.text, "html5lib")
    # for sezon in bs_2.find_all(text='Sezon'):
    #     print(sezon)
    # season = driver.find_element_by_xpath('//*[@id="node_main-wrap"]/div[5]/div[1]/div/div/div[2]/div/div/div[1]/div[1]/div[1]/div/div/div[1]/div/div[1]')
    # print(season.get_attribute("text"))
    time.sleep(3)
    # book.save('statistic.xlsx')
    time.sleep(4)
    arrow = driver.find_element_by_class_name('arrow')
    arrow.click()
    time.sleep(4)
    choose = driver.find_element_by_xpath('//*[@id="node_main-wrap"]/div[5]/div[1]/div/div/div[2]/div/div/div[1]/div[1]/div[1]/div/div/div[1]/div/div[1]').text

    season_number = int(choose[-5:])
    book = openpyxl.load_workbook('probe.xlsx')
    sheet = book.active
    max = sheet.max_row
    max_copy = max
    for i in range(1):
        season_number -= 1
        str_season = 'Sezon '+ str(season_number)
        new_season = driver.find_elements_by_class_name('sb-option')
        new_season[i].click()
        time.sleep(10)
        # bs_2 = bs4.BeautifulSoup(script_of_side.text, "html5lib")
        item = driver.find_elements_by_class_name('ftx')
        for ind in item:
            str_wynik = ind.text
            # print(str_wynik)
            if len(str_wynik) == 0:
                 break
            str_cell_e = 'E' + str(max_copy)
            sheet[str_cell_e] = str_wynik
            if int(str_wynik[0]) == int(str_wynik[-1]):
                str_cell_f = 'F' + str(max_copy)
                sheet[str_cell_f] = 'Remis'
                # print('Remis')
            max_copy += 1
        # max_copy = max
        # pierwsza_polowa = driver.find_elements_by_class_name('p1')
        # for polowa in pierwsza_polowa:
        #     str_wynik_polowa = polowa.text
        #     # print(str_wynik)
        #     if len(str_wynik_polowa)==0:
        #         break
        #     if str_wynik_polowa[0] == 'P':
        #         continue
        #     str_cell_b = 'B' + str(max_copy)
        #     sheet[str_cell_b] = str_wynik_polowa
        #     if int(str_wynik_polowa[0]) == int(str_wynik_polowa[-1]):
        #         str_cell_c = 'C' + str(max_copy)
        #         sheet[str_cell_c] = 'Remis'
        #         # print('Remis')
        #     max_copy += 1
        max_copy = max
        home_winner = driver.find_elements_by_class_name('home')
        away_winner = driver.find_elements_by_class_name('away')
        for j in range(len(home_winner)):
            if j < len(away_winner):
                mecz = str(home_winner[j].text) + ' : ' + str(away_winner[j].text)
                # print(mecz)
                str_cell_a = 'A' + str(max_copy)
                sheet[str_cell_a] = mecz
                max_copy += 1

        arrow = driver.find_element_by_class_name('arrow')
        arrow.click()
        time.sleep(3)

        teams = ['MKS', 'Lew', 'Wiara', 'Łodzianie']
        for cell in sheet.iter_rows(max_col=1):
            if cell.value == re.compile(r'[MKS]{3}'):
                    print(sheet.cell(row=cell.row, column=3).value)

    book.save('probe.xlsx')
    driver.close()
    return True
